class Book {
  constructor(title, author, year) {
    this.title = title;
    this.author = author;
    this.year = year;
  }

  getSummary() {
    return `${this.title} was written by ${this.author} in ${this.year}`;
  }

  getAge() {
    const years = new Date().getFullYear() - this.year;
    return `${this.title} is ${years} years old`;
  }

  revise(newYear) {
    this.year = newYear;
    this.revised = true;
  }

  static topBookStore() {
    return "Barnes & Noble";
  }
}

// Instantiate Object
const book1 = new Book("Book One", "John Doe", "2013");

console.log("Books:");
console.log(book1);

console.log("Summary:");
console.log(book1.getSummary());

console.log("Age:");
console.log(book1.getAge());

console.log("Revise:");
console.log(book1);
book1.revise("2018");
console.log(book1);

console.log("Static Method:");
console.log(Book.topBookStore());
